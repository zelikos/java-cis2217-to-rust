import java.util.ArrayList;
import java.util.Collections;
import java.io.Serializable;


public class FacebookUser extends UserAccount implements Comparable<FacebookUser>, Serializable {
	
	private String passwordHint;
	private ArrayList<FacebookUser> friends;

	protected FacebookUser (String username, String password) {
		super(username, password);
		friends = new ArrayList<>();
	}
	
	public void setPasswordHint (String hint) {
		this.passwordHint = hint;
	}
	
	public boolean friend (FacebookUser newFriend) {
		boolean canBefriend = true;
		
		if (friends.size() != 0) {
			for (int i = 0; i < friends.size(); i++) {
				if (friends.get(i).equals(newFriend)) {
					System.out.println(newFriend.toString() + " is already friended.");
					canBefriend = false;
				}
			}
		}

		if (canBefriend) {
			friends.add(newFriend);
		}
		
		return canBefriend;
	}
	
	public boolean defriend (FacebookUser formerFriend) {
		boolean canDefriend = false;
		int friendPos = 0;
		
		if (friends.size() == 0) {
			System.out.println(this.toString() + " has no friends.");
		} else {
			for (int i = 0; i < friends.size(); i++) {
				if (friends.get(i).equals(formerFriend)) {
					canDefriend = true;
					friendPos = i;
					i = friends.size();
				}
			}
			
			if (canDefriend) {
				friends.remove(friendPos);
			} else if (!canDefriend) {
				System.out.println(formerFriend.toString() + " is not currently friended.");
			}
		}
		
		return canDefriend;
	}
	
	public int getNumFriends () {
		return friends.size();
	}
	
	public ArrayList<FacebookUser> getFriends () {
		ArrayList<FacebookUser> friendsList;
		
		friendsList = new ArrayList<>(friends);
		
		Collections.sort(friendsList);
		
		return friendsList;
	}

	@Override
	public void getPasswordHelp() {
		System.out.println(passwordHint);
	}

	@Override
	public int compareTo(FacebookUser o) {
		
		if (this.toString().compareToIgnoreCase(o.toString()) != 0) {
			return this.toString().compareToIgnoreCase(o.toString());
		}

		return 0;
	}

}
