import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Stack;
import java.util.TreeMap;
import java.io.Serializable;

/* EXPLANATION
 * For the tenth assignment, I used a Stack. Undo functionality works by 
 * starting with the last undo-able operation and back to the first undo-able
 * operation that was performed. A Stack works in that only the most-recently
 * added item is operated on, and so it is a perfect fit for implementing
 * a simple undo feature.
 * 
 * 
 * Undo-able operations:
 * **Add friend [x]
 * **Remove friend [x]
 * Add user [x]
 * Delete user [ ]
 * Like [x]
 * 
 * Data to track for undos:
 * 1. Operation performed
 * 2. Performed-by
 * 3. Performed on (otherUser for most, likedTopic for likes)
 * 
 * Methods that are undo-able now return the data required for undo operation,
 * rather than being void methods.
 * 
 */


/* EXPLANATION
 * For the seventh assignment, I used a TreeMap and a HashSet.
 * 
 * A Map was chosen so as to use each topic (or "liked thing") as a key,
 * and the list of users that liked the respective topic as the value.
 * 
 * A TreeMap was specifically chosen because the topics needed to be listed
 * in alphabetical order.
 * 
 * 
 * For tracking the number of likes for each topic, a HashSet was chosen.
 * While only the number of likes is presented to the user, storing a list of 
 * usernames that liked the topic--rather than an integer value--allows for 
 * easily checking whether one already liked the topic.
 * 
 * Using a Set makes it easy to avoid duplication, and as the order of users
 * in the list does not matter, a HashSet was chosen for this purpose.
 * 
 */

public class Facebook implements Serializable {
	enum ListOperation {
		ADD,
		REMOVE
	}
	
	enum SortMode {
		ALPHA,
		NUM_FRIENDS
	}
	
	private ArrayList<FacebookUser> users;
	private TreeMap<String, HashSet<String>> likedTopics;
	private Stack<String[]> undoableOps;
	
	protected Facebook () {
		users = new ArrayList<>();
		likedTopics = new TreeMap<>();
		undoableOps = new Stack<>();
	}
	
	private int findUser (String user) {
		int userPos = -1;
		
		if (users.size() != 0) {
			for (int i = 0; i < users.size(); i++) {
				if (users.get(i).toString().equals(user)) {
					userPos = i;
					i = users.size();
				}
			}
		}

		return userPos;
	}
	
	private boolean authUser (String user, int userPos) {
		String pass;
		Scanner input = new Scanner(System.in);

		if (userPos == -1) {
			System.out.println(user + " could not be found.");
			return false;
		}
		
		System.out.print("Password: ");
		pass = input.nextLine();
		if (users.get(userPos).checkPassword(pass)) {
			return true;
		} else {
			System.out.println("Incorrect password.");
			return false;
		}
	}
	
	private int checkFriend () {
		int friendPos = 0;
		String friendName;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Name of friend: ");
		friendName = input.nextLine();
		
		friendPos = findUser(friendName);

		return friendPos;
	}

	public void listUsers (SortMode sortBy) {
		if (users.size() == 0) {
			System.out.println("No registered users found.");
		} else {
			if (sortBy == SortMode.ALPHA) {
				Collections.sort(users);
			} else if (sortBy == SortMode.NUM_FRIENDS) {
				Collections.sort(users, Comparator.comparing(FacebookUser::getNumFriends).reversed());
			}
			for (int i = 0; i < users.size(); i++) {
				System.out.println(users.get(i).toString());
			}
		}
	}
	
	public String[] addUser (String newUser) {
		Scanner input = new Scanner(System.in);
		FacebookUser newFacebookUser;
		String newPass, passHint;
		String[] undoable = {"", "", ""};
		
		if (users.size() != 0) {
			if (findUser(newUser) != -1) {
				System.out.println(newUser + " is already registered.");
			}
		}

		if (findUser(newUser) == -1) {
			System.out.print("Password: ");
			newPass = input.nextLine();
			System.out.print("Password hint: ");
			passHint = input.nextLine();
			
			newFacebookUser = new FacebookUser(newUser, newPass);
			newFacebookUser.setPasswordHint(passHint);
			users.add(newFacebookUser);

			undoable[0] = "addUser";
			undoable[1] = newUser;
			undoable[2] = newUser;
		}
		
		return undoable;
	}
	
	public String[] delUser (String formerUser) {
		int userPos = 0;
		String[] undoable = {"", "", ""};

		userPos = findUser(formerUser);
		
		if (authUser(formerUser, userPos)) {
			users.remove(userPos);
			System.out.println(formerUser + " removed.");
			
			undoable[0] = "delUser";
			undoable[1] = formerUser;
			undoable[2] = formerUser;
		}
		
		return undoable;
	}
	
	public void displayPassHint (String user) {
		int userPos = 0;
		
		userPos = findUser(user);
		
		if (userPos != -1) {
			System.out.print("Hint: ");
			users.get(userPos).getPasswordHelp();
		}	
	}
	
	public String[] modFriendList (String user, ListOperation mode) {
		int userPos;
		FacebookUser currentUser, friend;
		String[] undoable = {"", "", ""};
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			currentUser = users.get(userPos);
			userPos = checkFriend();
			if (userPos != -1) {
				friend = users.get(userPos);
				if (mode == ListOperation.ADD) {
					if (currentUser.friend(friend)) {
						undoable[0] = "addFriend";
					}
				} else if (mode == ListOperation.REMOVE) {
					if (currentUser.defriend(friend)) {
						undoable[0] = "removeFriend";
					}
				}
				undoable[1] = user;
				undoable[2] = friend.toString();
			}
		}
		
		return undoable;
	}
	
	public void listFriends (String user) {
		int userPos;
		ArrayList<FacebookUser> friendsList;
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			friendsList = users.get(userPos).getFriends();
			for (int i = 0; i < friendsList.size(); i++) {
				System.out.println(friendsList.get(i).toString());
			}
		} 
	}
	
	public String[] addLike (String user) {
		HashSet<String> likedBy;
		Scanner input = new Scanner(System.in);
		String[] undoable = {"", "", ""};
		
		int userPos;
		String topic;
		boolean alreadyLiked = false;
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			System.out.print("What do you like? ");
			topic = input.nextLine();
			if (likedTopics.containsKey(topic)) {
				likedBy = likedTopics.get(topic);
				if (likedBy.contains(user)) {
					alreadyLiked = true;
				}
				likedBy.add(user);
			} else {
				likedBy = new HashSet<>();
				likedBy.add(user);
				likedTopics.put(topic, likedBy);
			}
			if (!alreadyLiked) {
				System.out.println(user + " likes " + topic + ".");
				undoable[0] = "addLike";
				undoable[1] = user;
				undoable[2] = topic;
			}
		}
		
		return undoable;
	}
	
	private boolean removeLike (String topic, String user) {
		boolean canRemove;
		
		if (likedTopics.containsKey(topic)) {
			canRemove = true;
			likedTopics.get(topic).remove(user);
			if (likedTopics.get(topic).size() == 0) {
				likedTopics.remove(topic);
			}
		} else {
			canRemove = false;
		}
		
		return canRemove;
	}
	
	public void listLikedTopics () {
		likedTopics.forEach((topic, likes) -> 
			System.out.println(topic + " (liked by " + likes.size() + " users)"));
	}

	private ArrayList<FacebookUser> getRecommendations (ArrayList<FacebookUser> recs, FacebookUser user) {
//	private void getRecommendations (ArrayList<FacebookUser> recs, FacebookUser user) {
		ArrayList<FacebookUser> friendsList = user.getFriends();
		FacebookUser friend;
		boolean onList = false;
		
		for (int i=0; i<friendsList.size(); i++) {
			friend = friendsList.get(i);
			
			if (recs.size() != 0) {
				for (int j=0; j<recs.size(); j++) {
					if (friend.equals(recs.get(j))) {
						onList = true;
					}
				}			
			} else if (recs.size() == 0) {
				onList = false;
			}

			if (!onList) {
				recs.add(friend);
//				System.out.println(friend + " added to recommendeds.");
				System.out.println(recs.size());
				getRecommendations(recs, friend);
			}
		}

		return recs;
	}
	
	public void listRecommendations (String user) {
		int userPos;
		FacebookUser currentUser;
		ArrayList<FacebookUser> recommendeds = new ArrayList<>();
//		ArrayList<FacebookUser> recommendeds;
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			currentUser = users.get(userPos);
//			getRecommendations(recommendeds, currentUser);
			recommendeds = getRecommendations(recommendeds, currentUser);
			Collections.sort(recommendeds, Comparator.comparing(FacebookUser::getNumFriends).reversed());
			System.out.println("Recommendations:");
			if (recommendeds.size() != 0) {
				for (int i=0; i<recommendeds.size(); i++) {
					System.out.println(recommendeds.get(i).toString());
				}
			}
		}
	}
	
	public void addUndoable (String[] undoable) {
		// If first String is empty, undoable operation was unsuccessful,
		// and so is not added to the stack.
		if (!(undoable[0].equals(""))) {
			undoableOps.push(undoable);
		}
	}
	
	public void undoLast () {
		if (undoableOps.empty()) {
			System.out.println("No actions available to undo.");
		} else {
			String[] lastOp = undoableOps.peek();
			String operation, user, actedOn;
			operation = lastOp[0];
			user = lastOp[1];
			actedOn = lastOp[2];
			
			int userPos = findUser(user), actedOnPos;
			
			
			switch (operation) {
				case "addFriend":
					actedOnPos = findUser(actedOn);
					System.out.println("Undoing \"Add a friend\" for user " + user);
					if (authUser(user, userPos)) {
						if (users.get(userPos).defriend(users.get(actedOnPos))) {
							undoableOps.pop();
							System.out.println("\"Add a friend\" undone.");
						}
					}
					break;
				case "removeFriend":
					actedOnPos = findUser(actedOn);
					System.out.println("Undoing \"Remove a friend\" for user " + user);
					if (authUser(user, userPos)) {
						if (users.get(userPos).friend(users.get(actedOnPos))) {
							undoableOps.pop();
							System.out.println("\"Remove a friend\" undone.");
						}
					}
					break;
				case "addLike":
					System.out.println("Undoing \"Like a topic\" for user " + user);
					if (authUser(user, userPos)) {
						if (removeLike(actedOn, user)) {
							undoableOps.pop();
							System.out.println("\"Like a topic\" undone.");
						}
					}
					break;
				case "addUser":
					System.out.println("Undoing \"Add user\" for user " + user);
					if (authUser(user, userPos)) {
						users.remove(userPos);
						undoableOps.pop();
						System.out.println("\"Add user\" undone.");
					}
					break;
				default:
					break;
			}
			
		}
	}
}
