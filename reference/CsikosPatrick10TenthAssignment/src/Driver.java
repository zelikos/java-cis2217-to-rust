import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Driver {
	private static final String FILENAME = "facebook.dat";
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		Facebook facebook = loadFacebook();
		String[] undoable = new String[3];

		while (true) {
			int choice = displayMenu();
			
			switch (choice) {
			case 1:
				facebook.listUsers(Facebook.SortMode.ALPHA);
				break;
			case 2:
				facebook.listUsers(Facebook.SortMode.NUM_FRIENDS);
				break;
			case 3:
				undoable = facebook.addUser(getUserName());
				facebook.addUndoable(undoable);
				break;
			case 4:
				undoable = facebook.delUser(getUserName());
				facebook.addUndoable(undoable);
				break;
			case 5:
				facebook.displayPassHint(getUserName());
				break;
			case 6:
				undoable = facebook.modFriendList(getUserName(), Facebook.ListOperation.ADD);
				facebook.addUndoable(undoable);
				break;
			case 7:
				undoable = facebook.modFriendList(getUserName(), Facebook.ListOperation.REMOVE);
				facebook.addUndoable(undoable);
				break;
			case 8:
				facebook.listFriends(getUserName());
				break;
			case 9:
				facebook.listRecommendations(getUserName());
				break;
			case 10:
				undoable = facebook.addLike(getUserName());
				facebook.addUndoable(undoable);
				break;
			case 11:
				facebook.listLikedTopics();
				break;
			case 12:
				facebook.undoLast();
				break;
			case 13:
				System.out.println("Exiting.");
				saveFacebook(facebook);
				System.exit(0);
			}
		}
	}

	private static Facebook loadFacebook () {
		Facebook facebook = null;
		boolean loadError = false;
		
		try {
			ObjectInputStream ois = new ObjectInputStream(
					new FileInputStream(FILENAME));
			
			facebook = (Facebook) ois.readObject();
			loadError = false;

			ois.close();
		} catch (FileNotFoundException e) {
			System.err.println("Could not open file \"" + FILENAME + "\"");
			loadError = true;
		} catch (IOException e) {
			System.err.println("Could not de-serialize the object");
			loadError = true;
		} catch (ClassNotFoundException e) {
			System.err.println("Could not cast the de-serialized object");
			loadError = true;
		}
		
		if (loadError) {
			facebook = new Facebook();
		}

		return facebook;
	}
	
	private static void saveFacebook (Facebook facebook) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(FILENAME));
			oos.writeObject(facebook);
			oos.close();
		} catch (FileNotFoundException e) {
			System.err.println("Could not create the file \"" + FILENAME + "\"");
		} catch (IOException e) {
			System.err.println("Could not serialize the object");
		}
	}
	
	private static String getUserName () {
		System.out.print("Username: ");
		return input.nextLine();
	}
	
	private static int displayMenu () {
		int choice = -1;
		
		while (choice < 1 || choice > 13) {
			System.out.println();
			System.out.println("Menu");
			System.out.println("1. List Users alphabetically");
			System.out.println("2. List Users by number of friends");
			System.out.println("3. Add a User");
			System.out.println("4. Delete a User");
			System.out.println("5. Get Password Hint");
			System.out.println("6. Add a friend");
			System.out.println("7. Remove a friend");
			System.out.println("8. List friends");
			System.out.println("9. Get friend recommendations");
			System.out.println("10. Like a topic");
			System.out.println("11. List liked topics alphabetically");
			System.out.println("12. Undo last action");
			System.out.println("13. Quit");
			System.out.println();
			System.out.print("> ");
			
			try {
				choice = input.nextInt();
				input.nextLine();
				
				if (choice < 1 || choice > 13) {
					System.out.println(choice + " is not a valid option.");
				}

			} catch (InputMismatchException e) {
				System.out.println("Choice must be an integer.");
				input.nextLine();
			}
		}
		
		return choice;
	}

}
