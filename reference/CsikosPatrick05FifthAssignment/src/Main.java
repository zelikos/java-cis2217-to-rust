import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		int intKey = 7;
		double doubleKey = 4.2;
		String stringKey = "key";
		
		FacebookUser account1 = new FacebookUser("james", "123456");
		FacebookUser account2 = new FacebookUser("watson", "18871927");
		FacebookUser account3 = new FacebookUser("ripley", "19790525");
		
		ArrayList<Integer> ints = new ArrayList<>();
		ArrayList<Double> doubles = new ArrayList<>();
		ArrayList<String> strings = new ArrayList<>();
		ArrayList<FacebookUser> users = new ArrayList<>();

		ArrayList<String> emptyList = new ArrayList<>();
		ArrayList<Integer> singleItemList = new ArrayList<>();

		ArrayList<FacebookUser> allDuplicates = new ArrayList<>();

		
		ints.add(1);
		ints.add(3);
		ints.add(4);
		ints.add(7);
		ints.add(1);
		ints.add(7);
		
		doubles.add(2.2);
		doubles.add(4.1);
		doubles.add(2.2);
		doubles.add(3.8);
		
		strings.add("this");
		strings.add("list");
		strings.add("contains");
		strings.add("key");
		strings.add("information");
		strings.add("about");
		strings.add("key");
		strings.add("information");
		
		users.add(account1);
		users.add(account2);
		users.add(account3);
		users.add(account2);
		users.add(account1);
		
		singleItemList.add(9);
		
		allDuplicates.add(account1);
		allDuplicates.add(account1);
		allDuplicates.add(account1);
		allDuplicates.add(account1);
		allDuplicates.add(account1);
		

		// Integers
		
		System.out.print("Integers list: ");
		displayList(ints);
		System.out.println();
		
		System.out.print("De-duped Integers: ");
		displayList(Utilities.removeDuplicates(ints));
		System.out.println();
		performSearch(ints, intKey);
		
		System.out.println();

		
		// Doubles
		
		System.out.print("Doubles list: ");
		displayList(doubles);
		System.out.println();
		
		System.out.print("De-duped Doubles: ");
		displayList(Utilities.removeDuplicates(doubles));
		System.out.println();
		performSearch(doubles, doubleKey);
		
		System.out.println();
		
		
		// Strings
		
		System.out.print("Strings list: ");
		displayList(strings);
		System.out.println();
		
		System.out.print("De-duped Strings: ");
		displayList(Utilities.removeDuplicates(strings));
		System.out.println();
		performSearch(strings, stringKey);
		
		System.out.println();
		
		
		// FacebookUsers
		
		System.out.print("FacebookUsers list: ");
		displayList(users);
		System.out.println();
		
		System.out.print("De-duped FacebookUsers: ");
		displayList(Utilities.removeDuplicates(users));
		System.out.println();
		performSearch(users, account2);
		
		System.out.println();
		
		
		// Empty list
		
		System.out.print("Empty list: ");
		displayList(emptyList);
		System.out.println();
		
		System.out.print("De-duped Empty: ");
		displayList(Utilities.removeDuplicates(emptyList));
		System.out.println();
		performSearch(emptyList, stringKey);
		
		System.out.println();
		
		
		// List containing a single item
		
		System.out.print("Single-item list: ");
		displayList(singleItemList);
		System.out.println();
		
		System.out.print("De-duped Single-item: ");
		displayList(Utilities.removeDuplicates(singleItemList));
		System.out.println();
		performSearch(singleItemList, intKey);
		
		System.out.println();
		
		
		// List containing all duplicates of the same item
		
		System.out.print("All-Duplicates list: ");
		displayList(allDuplicates);
		System.out.println();
		
		System.out.print("De-duped All-Duplicates: ");
		displayList(Utilities.removeDuplicates(allDuplicates));
		System.out.println();
		performSearch(allDuplicates, account3);
		
		System.out.println();
		
	}
	
	public static <E> void displayList (ArrayList<E> list) {
		if (list.size() != 0) {
			for (int i=0; i<list.size(); i++) {
				System.out.print(list.get(i) + "  ");
			}
		}
	}
	
	public static <E extends Comparable<E>> void performSearch (ArrayList<E> list, E key) {
		int pos;
		
		System.out.println("Searching for " + key + " in list.");
		pos = Utilities.linearSearch(list, key);
		if (pos == -1) {
			System.out.println(key + " not found in list.");
		} else {
			System.out.println(key + " found at index " + pos + ".");
		}
	}

}
