import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.Scanner;
import java.io.Serializable;

/* EXPLANATION
 * For this assignment, I used a TreeMap and a HashSet.
 * 
 * A Map was chosen so as to use each topic (or "liked thing") as a key,
 * and the list of users that liked the respective topic as the value.
 * 
 * A TreeMap was specifically chosen because the topics needed to be listed
 * in alphabetical order.
 * 
 * 
 * For tracking the number of likes for each topic, a HashSet was chosen.
 * While only the number of likes is presented to the user, storing a list of 
 * usernames that liked the topic--rather than an integer value--allows for 
 * easily checking whether one already liked the topic.
 * 
 * Using a Set makes it easy to avoid duplication, and as the order of users
 * in the list does not matter, a HashSet was chosen for this purpose.
 * 
 */

public class Facebook implements Serializable {
	enum ListOperation {
		ADD,
		REMOVE
	}
	
	enum SortMode {
		ALPHA,
		NUM_FRIENDS
	}
	
	private ArrayList<FacebookUser> users;
	private TreeMap<String, HashSet<String>> likedTopics;
	
	protected Facebook () {
		users = new ArrayList<>();
		likedTopics = new TreeMap<>();
	}
	
	private int findUser (String user) {
		int userPos = -1;
		
		if (users.size() != 0) {
			for (int i = 0; i < users.size(); i++) {
				if (users.get(i).toString().equals(user)) {
					userPos = i;
					i = users.size();
				}
			}
		}

		return userPos;
	}
	
	private boolean authUser (String user, int userPos) {
		String pass;
		Scanner input = new Scanner(System.in);

		if (userPos == -1) {
			System.out.println(user + " could not be found.");
			return false;
		}
		
		System.out.print("Password: ");
		pass = input.nextLine();
		if (users.get(userPos).checkPassword(pass)) {
			return true;
		} else {
			System.out.println("Incorrect password.");
			return false;
		}
	}
	
	private int checkFriend () {
		int friendPos = 0;
		String friendName;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Name of friend: ");
		friendName = input.nextLine();
		
		friendPos = findUser(friendName);

		return friendPos;
	}

	public void listUsers (SortMode sortBy) {
		if (users.size() == 0) {
			System.out.println("No registered users found.");
		} else {
			if (sortBy == SortMode.ALPHA) {
				Collections.sort(users);
			} else if (sortBy == SortMode.NUM_FRIENDS) {
				Collections.sort(users, Comparator.comparing(FacebookUser::getNumFriends).reversed());
			}
			for (int i = 0; i < users.size(); i++) {
				System.out.println(users.get(i).toString());
			}
		}
	}
	
	public void addUser (String newUser) {
		Scanner input = new Scanner(System.in);
		FacebookUser newFacebookUser;
		String newPass, passHint;
		
		if (users.size() != 0) {
			if (findUser(newUser) != -1) {
				System.out.println(newUser + " is already registered.");
			}
		}

		if (findUser(newUser) == -1) {
			System.out.print("Password: ");
			newPass = input.nextLine();
			System.out.print("Password hint: ");
			passHint = input.nextLine();
			
			newFacebookUser = new FacebookUser(newUser, newPass);
			newFacebookUser.setPasswordHint(passHint);
			users.add(newFacebookUser);
		}
	}
	
	public void delUser (String formerUser) {
		int userPos = 0;

		userPos = findUser(formerUser);
		
		if (authUser(formerUser, userPos)) {
			users.remove(userPos);
			System.out.println(formerUser + " removed.");
		}
	}
	
	public void displayPassHint (String user) {
		int userPos = 0;
		
		userPos = findUser(user);
		
		if (userPos != -1) {
			System.out.print("Hint: ");
			users.get(userPos).getPasswordHelp();
		}	
	}
	
	public void modFriendList (String user, ListOperation mode) {
		int userPos;
		FacebookUser currentUser, friend;
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			currentUser = users.get(userPos);
			userPos = checkFriend();
			if (userPos != -1) {
				friend = users.get(userPos);
				if (mode == ListOperation.ADD) {
					currentUser.friend(friend);
				} else if (mode == ListOperation.REMOVE) {
					currentUser.defriend(friend);
				}
			}
		}
	}
	
	public void listFriends (String user) {
		int userPos;
		ArrayList<FacebookUser> friendsList;
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			friendsList = users.get(userPos).getFriends();
			for (int i = 0; i < friendsList.size(); i++) {
				System.out.println(friendsList.get(i).toString());
			}
		} 
	}
	
	public void addLike (String user) {
		HashSet<String> likedBy;
		Scanner input = new Scanner(System.in);
		
		int userPos;
		String topic;
		boolean alreadyLiked = false;
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			System.out.print("What do you like? ");
			topic = input.nextLine();
			if (likedTopics.containsKey(topic)) {
				likedBy = likedTopics.get(topic);
				if (likedBy.contains(user)) {
					alreadyLiked = true;
				}
				likedBy.add(user);
			} else {
				likedBy = new HashSet<>();
				likedBy.add(user);
				likedTopics.put(topic, likedBy);
			}
			if (!alreadyLiked) {
				System.out.println(user + " likes " + topic + ".");
			}
		}
	}
	
	public void listLikedTopics () {
		likedTopics.forEach((topic, likes) -> 
			System.out.println(topic + " (liked by " + likes.size() + " users)"));
	}

	private ArrayList<FacebookUser> getRecommendations (ArrayList<FacebookUser> recs, FacebookUser user) {
//	private void getRecommendations (ArrayList<FacebookUser> recs, FacebookUser user) {
		ArrayList<FacebookUser> friendsList = user.getFriends();
		FacebookUser friend;
		boolean onList = false;
		
		for (int i=0; i<friendsList.size(); i++) {
			friend = friendsList.get(i);
			
			if (recs.size() != 0) {
				for (int j=0; j<recs.size(); j++) {
					if (friend.equals(recs.get(j))) {
						onList = true;
					}
				}			
			} else if (recs.size() == 0) {
				onList = false;
			}

			if (!onList) {
				recs.add(friend);
//				System.out.println(friend + " added to recommendeds.");
				System.out.println(recs.size());
				getRecommendations(recs, friend);
			}
		}

		return recs;
	}
	
	public void listRecommendations (String user) {
		int userPos;
		FacebookUser currentUser;
		ArrayList<FacebookUser> recommendeds = new ArrayList<>();
//		ArrayList<FacebookUser> recommendeds;
		
		userPos = findUser(user);
		
		if (authUser(user, userPos)) {
			currentUser = users.get(userPos);
//			getRecommendations(recommendeds, currentUser);
			recommendeds = getRecommendations(recommendeds, currentUser);
			Collections.sort(recommendeds, Comparator.comparing(FacebookUser::getNumFriends).reversed());
			System.out.println("Recommendations:");
			if (recommendeds.size() != 0) {
				for (int i=0; i<recommendeds.size(); i++) {
					System.out.println(recommendeds.get(i).toString());
				}
			}
		}
	}
}
