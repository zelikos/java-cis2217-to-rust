import java.util.ArrayList;

public class Utilities<E> {

	public static <E> ArrayList<E> removeDuplicates (ArrayList<E> list) {
		ArrayList<E> deDuped = new ArrayList<>();
		boolean found = false;
		
		if (list.size() > 0) {
			// If list is not empty, always add first item to de-duplicated list
			deDuped.add(list.get(0));

			if (list.size() > 1) {
				// Since first item is always added, loop starts on second item
				// if the list contains more than one item
				for (int i=1; i<list.size(); i++) {
					found = false;
					for (int j=0; j<deDuped.size(); j++) {
						if (list.get(i).equals(deDuped.get(j))) {
							found = true;
							j = deDuped.size();
						}
					}

					if (!found) {
						deDuped.add(list.get(i));
					}
				}
			}

		}
		
		return deDuped;
	}
	
	public static <E extends Comparable<E>> int linearSearch (ArrayList<E> list, E key) {
		int pos = -1;
		
		if (list.size() > 0) {
			for (int i=0; i<list.size(); i++) {
				if (list.get(i).equals(key)) {
					pos = i;
					i = list.size(); // Ends loop once found so that pos == first instance of item
				}
			}
		}

		return pos;
	}
}
