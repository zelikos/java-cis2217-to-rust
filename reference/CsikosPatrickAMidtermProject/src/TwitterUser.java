import java.util.ArrayList;
import java.util.Collections;

public class TwitterUser extends UserAccount implements Comparable<TwitterUser>, Cloneable {
	
	private ArrayList<TwitterUser> follows;
	
	protected TwitterUser (String username) {
		super (username, "");
		follows = new ArrayList<>();
	}
	
	public void follow (TwitterUser newFollow) {
		boolean alreadyFollowed = false;
		
		if (follows.size() != 0) {
			for (int i = 0; i < follows.size(); i++) {
				if (follows.get(i).equals(newFollow)) {
//					System.out.println(newFollow.toString() + " is already friended.");
					alreadyFollowed = true;
				}
			}
		}

		if (!alreadyFollowed) {
			follows.add(newFollow);
		}
	}
	
	public ArrayList<TwitterUser> getFollows () {
		ArrayList<TwitterUser> followsList;
		
		followsList = new ArrayList<>(follows);
		
		Collections.sort(followsList);
		
		return followsList;
	}
	
	public void clearFollows () {
		follows.clear();
	}
	
	@Override
	public Object clone () throws CloneNotSupportedException {
		TwitterUser userClone = (TwitterUser) super.clone();
		
		ArrayList<TwitterUser> followsClone = new ArrayList<>();
		
		for (int i=0; i<userClone.follows.size(); i++) {
			followsClone.add((TwitterUser) userClone.follows.get(i));
		}
		
		userClone.follows = followsClone;
		
		return userClone;
	}
	
	@Override
	public void getPasswordHelp () {
		//
	}
	
	@Override
	public int compareTo (TwitterUser o) {
		Integer user1 = Integer.parseInt(this.toString());
		Integer user2 = Integer.parseInt(o.toString());
		
		if (user1.compareTo(user2) != 0) {
			return user1.compareTo(user2);
		}
		
		return 0;
	}
}
