import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Driver {
	private static final String FILENAME = "social_network.edgelist";

	private static ArrayList<TwitterUser> users = new ArrayList<>();
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		ArrayList<TwitterUser> cloneFollows;
		TwitterUser userClone, testUser;
		String userTest1 = "0", userTest2 = "", choice = "";
		int depth = 0, userTestID;
		boolean done = false;

		loadUserList();
		
		System.out.println("Testing getNeighborhood...");
		
		while (!done) {
			System.out.print("User ID to test: ");
	
			try {
				userTest2 = Integer.toString(input.nextInt());
				input.nextLine();
			} catch (InputMismatchException e) {
				System.err.println("User ID must be an integer.");
				input.nextLine();
				continue;
			}
			
			System.out.print("Depth: ");
			
			try {
				depth = input.nextInt();
				input.nextLine();
			} catch (InputMismatchException e) {
				System.err.println("Depth must be an integer.");
				input.nextLine();
				continue;
			}
			
			System.out.println("User " + userTest2 + " with depth of " + depth);
			listNeighborhood(userTest2, depth);
			
			System.out.println();
			System.out.print("Test another? (y/n): ");
			choice = input.nextLine();
			if (choice.equals("y") || choice.equals("Y")) {
				done = false;
			} else {
				done = true;
			}
			
		}
		
		System.out.println();
		System.out.println("Cloning user " + userTest1 + "...");
		
		testUser = users.get(binUserSearch(userTest1));
		
		try {
			userClone = (TwitterUser) testUser.clone();
			
			userClone.clearFollows();
			
			System.out.println(userTest1 + " is following:");
			listFollows(userTest1);
			
			System.out.println(userTest1 + "'s clone is following:");
			
			cloneFollows = userClone.getFollows();
			
			if (cloneFollows.size() != 0) {
				for (int i=0; i<cloneFollows.size(); i++) {
					System.out.println(cloneFollows.get(i).toString());
				}
			}
			
		} catch (CloneNotSupportedException e) {
			System.err.println("Object could not be cloned.");
		}
		
//		listFollows(userTest);
	}
	
	private static void listFollows (String user) {
		int userPos = binUserSearch(user);
		if (userPos != -1) {
			ArrayList<TwitterUser> follows = users.get(userPos).getFollows();
			
			for (int i=0; i<follows.size(); i++) {
				System.out.println(follows.get(i).toString());
			}
		} 
		
	}
	
//	private static int findUser (String user) {
//		// TODO: convert to binary search
//		int userPos = -1;
//		
//		if (users.size() != 0) {
//			for (int i = 0; i < users.size(); i++) {
//				if (users.get(i).toString().equals(user)) {
//					userPos = i;
//					i = users.size();
//				}
//			}
//		}
//
//		return userPos;
//	}
	
	private static int binUserSearch (String user) {
		int userPos = -1;
		int low = 0, high = users.size(), mid;
		int key = Integer.parseInt(user);
		
		boolean posFound = false;
		
		while (!posFound) {
			if (high < low) {
				posFound = true;
				userPos = -1;
			} else {
				mid = low + (high - low) / 2;
				
				int midVal = Integer.parseInt(users.get(mid).toString());
				
				if (midVal < key) {
					low = mid + 1;
				}
				
				if (midVal > key) {
					high = mid - 1;
				}
				
				if (midVal == key) {
					userPos = mid;
					posFound = true;
				}
			}
			
		}
		
		return userPos;
	}
	
	private static int findSpace (String line) {
		for (int i=0; i<line.length(); i++) {
			if (line.charAt(i) == ' ') {
				return i;
			}
		}
		
		return -1;
	}
	
//	private static void addUser (String username) {
//		if (binUserSearch(username) == -1) {
//			TwitterUser newUser = new TwitterUser(username);
//			users.add(newUser);
//		}
//	}
	
	private static ArrayList<String> loadUserFile () {
		ArrayList<String> rawUserList = new ArrayList<>();
		String currLine;
		
		try {
			System.out.println("Loading file " + FILENAME + "...");
			File inFile = null;
			
			JFileChooser chooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Edgelist file", "edgelist");
			chooser.setFileFilter(filter);
			
			int returnVal = chooser.showOpenDialog(null);
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				inFile = chooser.getSelectedFile();
			}
			
			if (inFile == null) {
				System.err.println("File " + FILENAME + " could not be found.");
				System.exit(0);
			}
			
			if (!chooser.getSelectedFile().getName().equals(FILENAME)) {
				System.err.println("Incorrect file selected.");
				System.exit(0);
			}
			
			BufferedReader buffIn = new BufferedReader(new FileReader(inFile));
			
			while(buffIn.ready()) {
				currLine = buffIn.readLine();
				rawUserList.add(currLine);
			}
			
			System.out.println("Loading complete.");
			System.out.println();
			
			buffIn.close();
			
		} catch (FileNotFoundException e) {
			System.err.println("File " + FILENAME + " could not be found.");
			System.exit(0);
		} catch (IOException e) {
			System.err.println("File " + FILENAME + " could not be loaded.");
			System.exit(0);
		}
		
		return rawUserList;
	}

	private static void loadUserList () {
		ArrayList<TwitterUser> userList = new ArrayList<>();
		ArrayList<String> loadedFile;
		
		loadedFile = loadUserFile();
		
		String currLine, user1, user2, currUser = "";
		int spacePos, user2Pos = -1;
		
		System.out.println("Generating user list...");
		
		for (int i=0; i<loadedFile.size(); i++) {
			currLine = loadedFile.get(i);
			spacePos = findSpace(currLine);

			if (spacePos != -1) {
				currUser = currLine.substring(0, spacePos);
				if (userList.size() > 0) {
					if (!currUser.equals(userList.get(userList.size()-1).toString())) {
						userList.add(new TwitterUser(currUser));
//						System.out.println("Added " + currUser);
					}
				} else if (userList.size() == 0) {
					userList.add(new TwitterUser(currUser));
				}
			}
		}

		users = userList;
		Collections.sort(users);
		
		System.out.println("Adding follows...");
		
		int filePos = 0;
		
		for (int i=0; i<users.size(); i++) {
//		for (int i=0; i<5; i++) {
			currUser = users.get(i).toString();

			do {
				currLine = loadedFile.get(filePos).toString();
				spacePos = findSpace(currLine);

				user1 = currLine.substring(0, spacePos);
				user2 = currLine.substring(spacePos+1);
				
//				System.out.println("currUser: " + currUser);
//				System.out.println("currLine: " + currLine);
//				System.out.println("User following is " + user1);
//				System.out.println("User followed is " + user2);
				
				if (currUser.equals(user1)) {
					user2Pos = binUserSearch(user2);
//					System.out.println(users.get(i).toString() + " following " + users.get(user2Pos).toString());
					users.get(i).follow(users.get(user2Pos));
				} else if (!currUser.equals(user1)) {
					filePos--;
				}
				
				filePos++;
			} while (filePos < loadedFile.size() && currUser.equals(user1));
			
//			System.out.println("Follows for " + currUser + " added.");
		}
		
		System.out.println("Setup complete.");
		System.out.println();

	}
	
	private static ArrayList<TwitterUser> getNeighborhood
	(ArrayList<TwitterUser> neighbors, TwitterUser user, int maxDepth, int currentDepth) {
		ArrayList<TwitterUser> followsList = user.getFollows();
		TwitterUser followee;
		boolean onList = false;
				
		for (int i=0; i<followsList.size(); i++) {
			followee = followsList.get(i);
			
			if (neighbors.size() != 0) {
				for (int j=0; j<neighbors.size(); j++) {
					if (followee.equals(neighbors.get(j))) {
						onList = true;
					}
				}			
			} else if (neighbors.size() == 0) {
			onList = false;
			}

			if (!onList) {
				neighbors.add(followee);
//				System.out.println(friend + " added to recommendeds.");
//				System.out.println(neighbors.size());
				if (currentDepth < maxDepth) {
					currentDepth++;
					neighbors = getNeighborhood(neighbors, followee, maxDepth, currentDepth);
				}
			}
		}

		return neighbors;
	}
	
	private static void listNeighborhood (String user, int depth) {
		int userPos, startDepth = 1;
		TwitterUser currentUser;
		ArrayList<TwitterUser> neighborhood = new ArrayList<>();
		
		userPos = binUserSearch(user);

		currentUser = users.get(userPos);
		neighborhood = getNeighborhood(neighborhood, currentUser, depth, startDepth);
		Collections.sort(neighborhood);
		System.out.println("Neighborhood sorted by ID:");
		if (neighborhood.size() != 0) {
			for (int i=0; i<neighborhood.size(); i++) {
				System.out.println(neighborhood.get(i).toString());
			}
		}
	}
	
}