import java.util.ArrayList;
import java.util.Collections;

public class Driver {

	public static void main(String[] args) {
		ArrayList<FacebookUser> accountsList = new ArrayList<>();
		
		FacebookUser account1 = new FacebookUser("james", "123456");
		FacebookUser account2 = new FacebookUser("watson", "18871927");
		FacebookUser account3 = new FacebookUser("ripley", "19790525");
		FacebookUser account4 = new FacebookUser("hans_bacc", "20151218");
		FacebookUser account5 = new FacebookUser("stark", "20190426");
		
		accountsList.add(account1);
		accountsList.add(account2);
		accountsList.add(account3);
		accountsList.add(account4);
		accountsList.add(account5);
		
		System.out.println("Accounts (unsorted): ");
		displayAccounts(accountsList);
		System.out.println();

		System.out.println("Accounts (sorted): ");
		Collections.sort(accountsList);
		displayAccounts(accountsList);
		System.out.println();
		
		
		System.out.println("Setting password hint for " + account5.toString());
		account5.setPasswordHint("Endgame");
		
		System.out.print("Hint for password of " + account5.toString() + ": ");
		account5.getPasswordHelp();
		System.out.println();
		
		account5.defriend(account1);
		System.out.println("Adding friends for " + account5.toString());
		account5.friend(account2);
		account5.friend(account3);
		account5.friend(account4);
		account5.friend(account4);
		
		System.out.println("Friends of " + account5.toString() + ":");
		displayAccounts(account5.getFriends());
		System.out.println();
		
		System.out.println("Removing friends of " + account5.toString());
		account5.defriend(account1);
		account5.defriend(account4);
		
		System.out.println("Friends of " + account5.toString() + ":");
		displayAccounts(account5.getFriends());
	}
	
	public static void displayAccounts (ArrayList<FacebookUser> accounts) {
		for (int i=0; i<accounts.size(); i++) {
			System.out.println(accounts.get(i).toString());
		}
	}

}
