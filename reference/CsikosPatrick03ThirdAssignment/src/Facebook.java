import java.util.ArrayList;
import java.util.Scanner;
import java.io.Serializable;


public class Facebook implements Serializable {
	private ArrayList<FacebookUser> users;
	
	protected Facebook () {
		users = new ArrayList<>();
	}
	
	private int findUser (String user) {
		int userPos = -1;
		
		if (users.size() != 0) {
			for (int i = 0; i < users.size(); i++) {
				if (users.get(i).toString().equals(user)) {
					userPos = i;
					i = users.size();
				}
			}
		}

		return userPos;
	}
	
	public void listUsers () {
		if (users.size() == 0) {
			System.out.println("No registered users found.");
		} else {
			for (int i = 0; i < users.size(); i++) {
				System.out.println(users.get(i).toString());
			}
		}
	}
	
	public void addUser (String newUser) {
		Scanner input = new Scanner(System.in);
		FacebookUser newFacebookUser;
		boolean alreadyRegistered = false;
		String newPass, passHint;
		
		if (users.size() != 0) {
			if (findUser(newUser) != -1) {
				System.out.println(newUser + " is already registered.");
				alreadyRegistered = true;
			}
		}

		if (!alreadyRegistered) {
			System.out.print("Password: ");
			newPass = input.nextLine();
			System.out.print("Password hint: ");
			passHint = input.nextLine();
			
			newFacebookUser = new FacebookUser(newUser, newPass);
			newFacebookUser.setPasswordHint(passHint);
			users.add(newFacebookUser);
		}
	}
	
	public void delUser (String formerUser) {
		int userPos = 0;
		String pass;
		Scanner input = new Scanner(System.in);
		
		userPos = findUser(formerUser);
		
		if (userPos != -1) {
			System.out.print("Password: ");
			pass = input.nextLine();
			if (users.get(userPos).checkPassword(pass)) {
				users.remove(userPos);
				System.out.println(formerUser + " removed.");
			} else {
				System.out.println("Incorrect password.");
			}
		} else {
			System.out.println(formerUser + " could not be found.");
		}
	}
	
	public void displayPassHint (String user) {
		int userPos = 0;
		
		userPos = findUser(user);
		
		if (userPos != -1) {
			System.out.print("Hint: ");
			users.get(userPos).getPasswordHelp();
		} else {
			System.out.println(user + " could not be found.");
		}
			
	}
}
