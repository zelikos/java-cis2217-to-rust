import java.util.ArrayList;
import java.util.Collections;
import java.io.Serializable;


public class FacebookUser extends UserAccount implements Comparable<FacebookUser>, Serializable {
	
	private String passwordHint;
	private ArrayList<FacebookUser> friends;

	protected FacebookUser (String username, String password) {
		super(username, password);
		friends = new ArrayList<>();
	}
	
	public void setPasswordHint (String hint) {
		this.passwordHint = hint;
	}
	
	public void friend (FacebookUser newFriend) {
		boolean alreadyFriended = false;
		
		if (friends.size() != 0) {
			for (int i = 0; i < friends.size(); i++) {
				if (friends.get(i).equals(newFriend)) {
					System.out.println(newFriend.toString() + " is already friended.");
					alreadyFriended = true;
				}
			}
		}

		if (!alreadyFriended) {
			friends.add(newFriend);
		}
	}
	
	public void defriend (FacebookUser formerFriend) {
		boolean friendFound = false;
		int friendPos = 0;
		
		if (friends.size() == 0) {
			System.out.println(this.toString() + " has no friends.");
		} else {
			for (int i = 0; i < friends.size(); i++) {
				if (friends.get(i).equals(formerFriend)) {
					friendFound = true;
					friendPos = i;
					i = friends.size();
				}
			}
			
			if (friendFound) {
				friends.remove(friendPos);
			} else if (!friendFound) {
				System.out.println(formerFriend.toString() + " is not currently friended.");
			}
		}

	}
	
	public ArrayList<FacebookUser> getFriends () {
		ArrayList<FacebookUser> friendsList;
		
		friendsList = new ArrayList<>(friends);
		
		Collections.sort(friendsList);
		
		return friendsList;
	}

	@Override
	public void getPasswordHelp() {
		System.out.println(passwordHint);
	}

	@Override
	public int compareTo(FacebookUser o) {
		
		if (this.toString().compareToIgnoreCase(o.toString()) != 0) {
			return this.toString().compareToIgnoreCase(o.toString());
		}

		return 0;
	}

}
