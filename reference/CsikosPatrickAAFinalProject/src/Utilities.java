import java.util.ArrayList;
import java.util.HashSet;

public class Utilities<E> {

//	removeDuplicates has been rewritten to use a HashSet for deduplication
//	instead of the iteration in the original version, resulting in much
//	faster deduplicating of elements than before.
	
	public static <E> ArrayList<E> removeDuplicates (ArrayList<E> list) {
//		ArrayList<E> deDuped = new ArrayList<>();
		HashSet<E> temp = new HashSet<>();
		boolean found = false;
		
//		if (list.size() > 0) {
//			// If list is not empty, always add first item to de-duplicated list
//			deDuped.add(list.get(0));
//
//			if (list.size() > 1) {
//				// Since first item is always added, loop starts on second item
//				// if the list contains more than one item
//				for (int i=1; i<list.size(); i++) {
//					found = false;
//					for (int j=0; j<deDuped.size(); j++) {
//						if (list.get(i).equals(deDuped.get(j))) {
//							found = true;
//							j = deDuped.size();
//						}
//					}
//
//					if (!found) {
//						deDuped.add(list.get(i));
//					}
//				}
//			}
//
//		}
		
		if (list.size() > 0) {
			for (E i : list) {
				temp.add(i);
			}
		}
		
		ArrayList<E> deDuped = new ArrayList<>(temp);
		temp.clear();
		return deDuped;
	}
	
	public static <E extends Comparable<E>> int linearSearch (ArrayList<E> list, E key) {
		int pos = -1;
		
		if (list.size() > 0) {
			for (int i=0; i<list.size(); i++) {
				if (list.get(i).equals(key)) {
					pos = i;
					i = list.size(); // Ends loop once found so that pos == first instance of item
				}
			}
		}

		return pos;
	}
}
