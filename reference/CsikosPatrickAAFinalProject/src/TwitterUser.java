import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;

public class TwitterUser extends UserAccount implements Comparable<TwitterUser>, Cloneable {
	
	private ArrayList<TwitterUser> follows;
	
//	An ArrayList to track the user's followers has been added to 
//	facilitate the `getFollowing` method in the Driver.
	private ArrayList<TwitterUser> followers;
	
//  The `follow`, `getFollows`, and `clone` methods were updated for the
//	changes listed above.
	
	protected TwitterUser (String username) {
		super (username, "");
		follows = new ArrayList<>();
		followers = new ArrayList<>();
	}
	
	public void follow (TwitterUser newFollow) {
		follows.add(newFollow);
	}
	
	public void addFollower (TwitterUser newFollower) {
		followers.add(newFollower);
	}
	
	
//	Removes duplicate entries and sorts the lists in ascending order.
	public void removeDups () {
		follows = Utilities.removeDuplicates(follows);
		Collections.sort(follows);
		
		followers = Utilities.removeDuplicates(followers);
		Collections.sort(followers);
	}
	
	public ArrayList<TwitterUser> getFollows () {
		ArrayList<TwitterUser> userFollows = new ArrayList<>(follows);

		return userFollows;
	}
	
	public ArrayList<TwitterUser> getFollowers () {
		ArrayList<TwitterUser> userFollowers = new ArrayList<>(followers);
		
		return userFollowers;
	}
	
//	getFollowCount and getFollowerCount are added to help facilitate the 
//	getByPopularity method in the Driver.
	
	public int getFollowCount () {
		return follows.size();
	}
	
	public int getFollowerCount () {
		return followers.size();
	}
	
	public void clearFollows () {
		follows.clear();
	}
	
	@Override
	public Object clone () throws CloneNotSupportedException {
		TwitterUser userClone = (TwitterUser) super.clone();
		
		ArrayList<TwitterUser> followsClone = new ArrayList<>();
		ArrayList<TwitterUser> followersClone = new ArrayList<>();

		for (TwitterUser i : follows) {
			followsClone.add(i);
		}
		
		for (TwitterUser i : followers) {
			followersClone.add(i);
		}

		userClone.follows = followsClone;
		userClone.followers = followersClone;
		
		return userClone;
	}
	
	@Override
	public void getPasswordHelp () {
		//
	}
	
	@Override
	public int compareTo (TwitterUser o) {
		Integer user1 = Integer.parseInt(this.toString());
		Integer user2 = Integer.parseInt(o.toString());
		
		if (user1.compareTo(user2) != 0) {
			return user1.compareTo(user2);
		}
		
		return 0;
	}
}
