import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
//import java.util.HashSet;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.io.IOException;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Driver {
	private static final String FILENAME = "social_network.edgelist";

//	Users list was changed from ArrayList to HashMap; the users list is not to
//	contain any duplicates, and using a HashMap allows for easier searching
//	of the objects within by using a String key.

//	usersByPopularity is used to contain the list of users sorted by popularity;
//	this is generated during initial setup in a `sortByPopularity' method.
	
	private static HashMap<String, TwitterUser> users = new HashMap<>();
	private static ArrayList<TwitterUser> usersByPopularity;
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		ArrayList<TwitterUser> followingList;
		TwitterUser userClone, testUser;
		String userTest1 = "0", userTest2 = "", choice = "";
		int depth = 0, userByPop, followerCount;
		boolean done = false;

		loadUserList();
		
		System.out.println("Testing getNeighborhood...");
		
		while (!done) {
			System.out.print("User ID to test: ");
	
			try {
				userTest2 = Integer.toString(input.nextInt());
				input.nextLine();
			} catch (InputMismatchException e) {
				System.err.println("User ID must be an integer.");
				input.nextLine();
				continue;
			}
			
			System.out.print("Depth: ");
			
			try {
				depth = input.nextInt();
				input.nextLine();
			} catch (InputMismatchException e) {
				System.err.println("Depth must be an integer.");
				input.nextLine();
				continue;
			}
			
			System.out.println("User " + userTest2 + " with depth of " + depth);
			listNeighborhood(userTest2, depth);
			
			System.out.println();
			System.out.print("Test another? (y/n): ");
			choice = input.nextLine();
			if (choice.equals("y") || choice.equals("Y")) {
				done = false;
			} else {
				done = true;
			}
			
		}
		
		System.out.println();
		System.out.println("Cloning user " + userTest1 + "...");
		
		testUser = users.get(userTest1);
		
		try {
			userClone = (TwitterUser) testUser.clone();
			
			userClone.clearFollows();
			
			System.out.println(userTest1 + " is following:");
			listFollows(userTest1);
			
			System.out.println(userTest1 + "'s clone is following:");

			for (TwitterUser i : userClone.getFollows()) {
				System.out.println(i.toString());
			}
			
		} catch (CloneNotSupportedException e) {
			System.err.println("Object could not be cloned.");
		}

//		Test getFollowing
		
		done = false;
		
		while (!done) {
			System.out.print("Select User ID to retrieve followers for: ");
			
			try {
				userTest2 = Integer.toString(input.nextInt());
				input.nextLine();
			} catch (InputMismatchException e) {
				System.err.println("User ID must be an integer.");
				input.nextLine();
				continue;
			}
			
			followerCount = users.get(userTest2).getFollowerCount();
			followingList = getFollowing(users.get(userTest2));
			
			System.out.println(userTest2 + "'s followers:");

			for (TwitterUser i : followingList) {
				System.out.println(i.toString());
			}
			
			System.out.println("Total followers for user " + userTest2 + ": " + followerCount);

			System.out.println();
			System.out.print("Select another? (y/n): ");
			choice = input.nextLine();
			if (choice.equals("y") || choice.equals("Y")) {
				done = false;
			} else {
				done = true;
			}

		}

//		Test getByPopularity
		
		done = false;
		
		while (!done) {
			System.out.print("Retrieve user by popularity rank: ");
			
			try {
				userByPop = input.nextInt();
				input.nextLine();
				
				if (userByPop <= 0) {
					System.out.println("Selection must be greater than 0.");
					continue;
				}
			} catch (InputMismatchException e) {
				System.err.println("Selection must be an integer.");
				input.nextLine();
				continue;
			}
			
			testUser = getByPopularity(userByPop);
			
			System.out.println("User " + testUser.toString() + " is at #"
							   + userByPop + " in popularity with "
							   + testUser.getFollowerCount() + " followers.");
			
			System.out.println();
			System.out.print("Retrieve another? (y/n): ");
			choice = input.nextLine();
			if (choice.equals("y") || choice.equals("Y")) {
				done = false;
			} else {
				done = true;
			}

		}
		
	}
	
	private static void listFollows (String user) {
		if (users.get(user) != null) {
			ArrayList<TwitterUser> follows = users.get(user).getFollows();
			
			for (TwitterUser i : follows) {
				System.out.println(i.toString());
			}
		} 
		
	}
	
//  Follower info is stored within the TwitterUser class in an ArrayList,
//	which is generated during the initial setup of the program. The ArrayList
//	has duplicate entries removed and is sorted in ascending order during
//	initial setup as well.
//	
//	The `getFollowers` method in TwitterUser returns a copy of the ArrayList.
//	
//	`getFollowing` as implemented below simply retrieves the follower list
//	of the requested user.
	
	private static ArrayList<TwitterUser> getFollowing(TwitterUser user) {
		String username = user.toString();

		return users.get(username).getFollowers();
	}
	
// The switch to a HashMap for the `users` obsoleted the need for the
// search methods that were present in the Midterm Project version,
// so they have been removed.
	
	private static int findSpace (String line) {
		for (int i=0; i<line.length(); i++) {
			if (line.charAt(i) == ' ') {
				return i;
			}
		}
		
		return -1;
	}
	
	private static ArrayList<String> loadUserFile () {
		ArrayList<String> rawUserList = new ArrayList<>();
		String currLine;
		
		try {
			System.out.println("Loading file " + FILENAME + "...");
			File inFile = null;
			
			JFileChooser chooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Edgelist file", "edgelist");
			chooser.setFileFilter(filter);
			
			int returnVal = chooser.showOpenDialog(null);
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				inFile = chooser.getSelectedFile();
			}
			
			if (inFile == null) {
				System.err.println("File " + FILENAME + " could not be found.");
				System.exit(0);
			}
			
			if (!chooser.getSelectedFile().getName().equals(FILENAME)) {
				System.err.println("Incorrect file selected.");
				System.exit(0);
			}
			
			BufferedReader buffIn = new BufferedReader(new FileReader(inFile));
			
			while(buffIn.ready()) {
				currLine = buffIn.readLine();
				rawUserList.add(currLine);
			}
			
			System.out.println("Loading complete.");
			System.out.println();
			
			buffIn.close();
			
		} catch (FileNotFoundException e) {
			System.err.println("File " + FILENAME + " could not be found.");
			System.exit(0);
		} catch (IOException e) {
			System.err.println("File " + FILENAME + " could not be loaded.");
			System.exit(0);
		}
		
		return rawUserList;
	}
	
//	Switching to a HashMap from an ArrayList allowed this method
//  to be greatly simplified from the Midterm Project version,
//  in large thanks to HashMap's inherent de-duplication of items,
//  and the ability to act on an object by the key it is mapped to.
//
//  The speed of the operation was vastly improved as well due to
//  this change.

	private static void loadUserList () {
		ArrayList<String> loadedFile;
		TwitterUser newUser;
		
		loadedFile = loadUserFile();
		
		String user1, user2;
		int spacePos;
		
		System.out.println("Generating user list...");
		
		for (String i : loadedFile) {
			spacePos = findSpace(i);

			if (spacePos != -1) {
				user1 = i.substring(0, spacePos);
				user2 = i.substring(spacePos+1);
				if (users.get(user1) == null) {
					newUser = new TwitterUser(user1);
					users.put(user1, newUser);
				}
				
				if (users.get(user2) == null) {
					newUser = new TwitterUser(user2);
					users.put(user2, newUser);
				}

				users.get(user1).follow(users.get(user2));
				users.get(user2).addFollower(users.get(user1));
			}
		}
		
		System.out.println("Processing users' follows and followers...");
		
		for (TwitterUser i : users.values()) {
			i.removeDups();
		}
	
//		System.out.println("Adding follows...");
		
//
//		for (String i : loadedFile) {
//			spacePos = findSpace(i);
//
//			user1 = i.substring(0, spacePos);
//			user2 = i.substring(spacePos+1);
//			
////			System.out.println("User 1 is " + user1);
////			System.out.println("User 2 is " + user2);
//
//			users.get(user1).follow(users.get(user2));
//			users.get(user2).addFollower(users.get(user1));
//		}
		
		System.out.println("Completing setup...");
		
		usersByPopularity = new ArrayList<>(users.values());
		
		usersByPopularity = sortByPopularity(usersByPopularity);
		
		System.out.println("Setup complete.");
		System.out.println();

	}

//	`getByPopularity` retrieves the specified user from a list of users that is 
//	sorted in a separate `sortByPopularity` method found below.
//	The `sortByPopularity` method is called during initial setup of the program,
//	and so `getByPopularity` does no sorting of its own; it simply retrieves
//	the specified user by their popularity ranking (i.e. 1 being the most popular)
//	from the pre-sorted list.
	
//	Assumption that x is >= 1
	private static TwitterUser getByPopularity (int x) {
		return usersByPopularity.get(x-1);
	}

	private static ArrayList<TwitterUser> sortByPopularity (ArrayList<TwitterUser> usersList) {
		usersList.sort(Comparator.comparing(TwitterUser::getFollowerCount)
				  				 .thenComparing(TwitterUser::getFollowCount)
				  				 .thenComparing(TwitterUser::toString)
				  				 .reversed());
		
		return usersList;
	}
	
//	While not listed in the Final Project's requirements, I also adjusted
//  the getNeighborhood method to use TreeSets so that the code could be
//  simplified as I wanted to correct the issues that were present in
//  the Midterm Project version.
	
	private static TreeSet<TwitterUser> getNeighborhood
	(TreeSet<TwitterUser> neighbors, TwitterUser user, int maxDepth, int currentDepth) {
		ArrayList<TwitterUser> followsList = user.getFollows();
		TwitterUser followee;

		for (TwitterUser i : followsList) {
			followee = i;
			
			neighbors.add(followee);
			if (currentDepth < maxDepth) {
				currentDepth++;
				neighbors = getNeighborhood(neighbors, followee, maxDepth, currentDepth);
			}
		}

		return neighbors;
	}
	
	private static void listNeighborhood (String user, int depth) {
		int startDepth = 1;
		TwitterUser currentUser;
		TreeSet<TwitterUser> neighborhood = new TreeSet<>();

		currentUser = users.get(user);
		neighborhood = getNeighborhood(neighborhood, currentUser, depth, startDepth);
		System.out.println("Neighborhood sorted by ID:");
		if (neighborhood.size() != 0) {
			for (TwitterUser i : neighborhood) {
				System.out.println(i.toString());
			}
		}
		
		System.out.println("Users in neighborhood of user " + user + ": " + neighborhood.size());
	}
	
}