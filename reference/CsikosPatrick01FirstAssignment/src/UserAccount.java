import java.util.Objects;

public class UserAccount {
	private String username;
	private String password;
	private boolean active; // indicates whether or not the account is currently active
	
	public UserAccount (String username, String password) {
		this.username = username;
		this.password = password;
		this.active = true;
	}
	
	public boolean getActive() {
		return active;
	}
	
	public boolean checkPassword (String password) {
		if (this.password.equals(password))
			return true;

		return false;
	}

	public void deactivateAccount () {
		active = false;
	}
	
	public String toString() {
		return username;
	}

	@Override
	public int hashCode() {
		return Objects.hash(username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAccount other = (UserAccount) obj;
		return Objects.equals(username, other.username);
	}

}
