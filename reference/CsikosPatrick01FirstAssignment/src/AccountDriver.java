
public class AccountDriver {

	public static void main(String[] args) {

		UserAccount account1 = new UserAccount("james", "123456");
		UserAccount account2 = new UserAccount("watson", "18871927");
		
		// Testing toString method
		
		System.out.println("account1: " + account1.toString());
		System.out.println("account2: " + account2.toString());
		System.out.println();
		
		// Testing deactivateAccount method
		
		if (account1.getActive()) {
			System.out.println("Account " + account1.toString() + " is active.");
		} else {
			System.out.println("Account " + account1.toString() + " is not active.");
		}
		
		System.out.println();
		account1.deactivateAccount();

		if (account1.getActive()) {
			System.out.println("Account " + account1.toString() + " is active.");
		} else {
			System.out.println("Account " + account1.toString() + " is not active.");
		}
		System.out.println();

		// Testing checkPassword method
		
		for (int i=0; i<2; i++) {
			String testPass = "";
			
			switch (i) {
			case 0:
				testPass = "potato";
				break;
			case 1:
				testPass = "18871927";
				break;
			}
			
			System.out.print("Testing '" + testPass + "': ");
			
			if (account2.checkPassword(testPass)) {
				System.out.println("Password matches.");
			} else {
				System.out.println("Password does not match.");
			}
			
			System.out.println();
		}
		
		// Testing equals method

		if (account1.equals(account2)) {
			System.out.println("The accounts have the same username.");
		} else {
			System.out.println("The accounts have different usernames.");
		}
	}

}
