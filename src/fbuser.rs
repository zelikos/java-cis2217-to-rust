use crate::useraccount::UserAccount;
use std::{hash::{Hash, Hasher}, collections::HashSet};

pub struct FacebookUser {
    username: String,
    password: String,
    active: bool,
    password_hint: String,
    friends: HashSet<String>
}

impl FacebookUser {
    pub fn set_password_hint (&mut self, hint: String) {
        self.password_hint = hint;
    }

    // Returns true if friend is not already in list
    pub fn friend (&mut self, friend_name: String) -> bool {
        let can_befriend = !(self.friends.contains(&friend_name));

        if can_befriend {
            self.friends.insert(friend_name);
        }

        can_befriend
    }

    pub fn defriend (&mut self, former_friend: &String) -> bool {
        let can_defriend = self.friends.contains(former_friend);

        if can_defriend {
            self.friends.remove(former_friend);
        }

        can_defriend
    }

    pub fn get_friends (&self) -> Vec<String> {
        let mut friends_list: Vec<String> = Vec::new();

        for friend in &self.friends {
            friends_list.push(friend.clone());
        }

        friends_list.sort_unstable();

        friends_list
    }
}

impl UserAccount for FacebookUser {
    fn new (username: String, password: String) -> Self {
        FacebookUser {
            username,
            password,
            active: true,
            password_hint: String::new(),
            friends: HashSet::new()
        }
    }

    fn get_active(&self) -> bool {
        self.active.clone()
    }

    fn get_password_help(&self) -> String {
        self.password_hint.clone()
    }

    fn check_password(&self, password: String) -> bool {
        if self.password == password {
            return true;
        } else {
            return false;
        }
    }

    fn deactivate_account(&mut self) {
        self.active = false;
    }

    fn to_string(&self) -> String {
        self.username.clone()
    }
}

// Hashing only based on username string, to match Java assignments
impl Hash for FacebookUser {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.username.hash(state);
    }
}

// Equality based on username string, to match Java assignments
impl PartialEq for FacebookUser {
    fn eq(&self, other: &Self) -> bool {
        self.username == other.username
    }
}

impl Eq for FacebookUser { }