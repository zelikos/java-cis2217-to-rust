use std::collections::HashMap;

use fbuser::FacebookUser;
use useraccount::UserAccount;

mod fbuser;
mod useraccount;

fn main() {
    let mut accounts_list: HashMap<String, FacebookUser> = HashMap::new();

    let account1_name = "james".to_owned();
    let account2_name = "watson".to_owned();
    let account3_name = "ripley".to_owned();
    let account4_name = "hans_bacc".to_owned();
    let account5_name = "stark".to_owned();

    let account1 = FacebookUser::new(account1_name.clone(), "123456".to_owned());
    let account2 = FacebookUser::new(account2_name.clone(), "18871927".to_owned());
    let account3 = FacebookUser::new(account3_name.clone(), "19790525".to_owned());
    let account4 = FacebookUser::new(account4_name.clone(), "20151218".to_owned());
    let account5 = FacebookUser::new(account5_name.clone(), "20190426".to_owned());

    accounts_list.insert(account1.to_string(), account1);
    accounts_list.insert(account2.to_string(), account2);
    accounts_list.insert(account3.to_string(), account3);
    accounts_list.insert(account4.to_string(), account4);
    accounts_list.insert(account5.to_string(), account5);

    println!("\nAccounts list (unsorted): ");
    display_accounts(&accounts_list);

    println!("\nAccounts list (sorted): ");
    display_accounts_sorted(&accounts_list);

    // TODO: better error-handling; low-priority
    let test_account =  accounts_list.get_mut(&account5_name).expect("Account not found");
    let test_account_name = test_account.to_string();

    println!("\nSetting password hint for {}...", test_account_name);

    test_account.set_password_hint("Endgame".to_owned());

    println!("\nHint for password of {}: {}", test_account_name, test_account.get_password_help());

    println!("\nAdding friends for {}", test_account_name);
    remove_friend(test_account, &account1_name);
    add_friend(test_account, &account2_name);
    add_friend(test_account, &account3_name);
    add_friend(test_account, &account4_name);
    add_friend(test_account, &account4_name);

    println!("\nFriends of {}:", test_account_name);

    for friend in test_account.get_friends() {
        println!("{}", friend);
    }

    println!("\nRemoving friends from {}", test_account_name);

    remove_friend(test_account, &account1_name);
    remove_friend(test_account, &account4_name);

    println!("\nFriends of {}:", test_account_name);

    for friend in test_account.get_friends() {
        println!("{}", friend);
    }
}

fn display_accounts (accounts: &HashMap<String, FacebookUser>) {
    for (account_name, _account) in accounts {
        println!("{}", account_name);
    }
}

fn display_accounts_sorted (accounts: &HashMap<String, FacebookUser>) {
    let mut sorted_accounts: Vec<String> = Vec::new();

    for (account_name, _account) in accounts {
        sorted_accounts.push(account_name.clone());
    }

    sorted_accounts.sort_unstable();

    for name in sorted_accounts {
        println!("{}", name);
    }
}

fn add_friend (account: &mut FacebookUser, friend_name: &String) {
    if account.friend(friend_name.clone())  == false {
        println!("{} is already on {}'s friend list.", friend_name, account.to_string());
    }
}

fn remove_friend (account: &mut FacebookUser, friend_name: &String) {
    if account.defriend(friend_name) == false {
        println!("{} is not on {}'s friend list.", friend_name, account.to_string());
    }
}