pub trait UserAccount {
    fn new (username: String, password: String) -> Self;

    fn get_active(&self) -> bool;

    fn get_password_help (&self) -> String;

    fn check_password(&self, password: String) -> bool;

    fn deactivate_account (&mut self);

    fn to_string (&self) -> String;
}